package com.example.miniobasics.Controller;

import com.google.api.client.util.IOUtils;
import com.jlefebure.spring.boot.minio.MinioException;
import com.jlefebure.spring.boot.minio.MinioService;
import io.minio.MinioClient;
import io.minio.errors.*;
import io.minio.messages.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.xmlpull.v1.XmlPullParserException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
/*
* August 01,2021
* */
@RestController
@RequestMapping("/files")
public class MinioController {

    @Autowired
    private MinioService minioService;

    @Autowired
    private MinioClient minioClient;

    @Value("testbucket")
    private String BucketName;

    @GetMapping
    public List<Item> testMinio() throws MinioException{
        return minioService.list();
    }

    @GetMapping("/{userfiles}")
    public void getObject(@PathVariable("userfiles") String object, HttpServletResponse response) throws MinioException, IOException,
            InvalidBucketNameException, InsufficientDataException, XmlPullParserException, ErrorResponseException,
            NoSuchAlgorithmException, NoResponseException, InvalidExpiresRangeException,
            InvalidKeyException, InternalException {
                    InputStream inputStream = minioService.get(Paths.get(object));
                    String url=minioClient.presignedGetObject(BucketName,object,60);

                    /*InputStreamResource inputStreamResource = new InputStreamResource(inputStream);

                    // Set the content type and attachment header.
                    response.addHeader("Content-disposition", "attachment;filename=" + object);
                    response.setContentType(URLConnection.guessContentTypeFromName(object));

                    // Copy the stream to the response's output stream.
                    IOUtils.copy(inputStream, response.getOutputStream());
                    response.flushBuffer();*/
                    response.sendRedirect(url);

    }

    @PostMapping
    public void addAttachement(@RequestParam("file") MultipartFile file) {
        Path path = Paths.get(file.getOriginalFilename());
        try {
            minioService.upload(path, file.getInputStream(), file.getContentType());
            System.out.println(file.getContentType());
            System.out.println();
            System.out.println(file.getInputStream());
        } catch (MinioException e) {
            throw new IllegalStateException("The file cannot be upload on the internal storage. Please retry later", e);
        } catch (IOException e) {
            throw new IllegalStateException("The file cannot be read", e);
        }

    }

    @GetMapping("/pre-signed/{object}")
    public String getObjectWithPresignedUrl(@PathVariable("object") String object) throws InvalidBucketNameException, InsufficientDataException, XmlPullParserException, ErrorResponseException, NoSuchAlgorithmException, IOException, NoResponseException, InvalidExpiresRangeException, InvalidKeyException, InternalException {
        return minioClient.presignedGetObject(BucketName,object,60);
    }
}
