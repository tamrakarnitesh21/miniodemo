package com.example.miniobasics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniobasicsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MiniobasicsApplication.class, args);
    }

}
